<?php

namespace Coro\AdminBundle\Admin;

use Coro\AdminBundle\Filter\ListMapper as FiltersMapper;
use Coro\AdminBundle\DataGrid\ListMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Coro\AdminBundle\Form\Type\MediaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\Type\ComponentsType;
use AppBundle\Form\Type\ComponentType;
use Api\Model\ProductTable;

class ProductAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields($formMapper) {
        $formMapper
            ->container('content', array(
                'html_tag' => 'div',
                'class' => 'row',
            ))
            ->panel('Ustawienia', array(
                'label' => 'Ustawienia',
                'active' => true,
                'general' => true,
                'class' => 'box-primary',
                'width' => 6,
            ))
            ->add('name', TextType::class, array(
                'label' => 'Nazwa'
            ))
            ->add('short_name', TextType::class, array(
                'label' => 'Krótka nazwa'
            ))
            ->add('hash', TextType::class, array(
                'label' => 'Hash'
            ))
            ->add('price', TextType::class, array(
                'label' => 'Cena'
            ))
            ->add('superprice', TextType::class, array(
                'label' => 'Cena promocyjna'
            ))
            ->add('active', CheckboxType::class, array(
                'label' => 'Aktywny',
                'required' => false,
            ))
            ->add('promo', CheckboxType::class, array(
                'label' => 'Promocja',
                'required' => false,
            ))
            ->add('editable', CheckboxType::class, array(
                'label' => 'Edytowalny',
                'required' => false,
            ))
            ->add('seo', TextType::class, array(
                'label' => 'Seo',
                'required' => true,
            ))
            ->end()
            ->panel('Media', array(
                'label' => 'Ikona',
                'active' => true,
                'general' => true,
                'class' => 'box-primary',
                'width' => 3,
            ))
            ->add('icon', MediaType::class, array())
            ->end()
            ->panel('Media2', array(
                'label' => 'Zdjęcie główne',
                'active' => true,
                'general' => true,
                'class' => 'box-primary',
                'width' => 3,
            ))
            ->add('picture', MediaType::class, array())
            ->end()
            ->end()
            ->container('row2', array(
                'html_tag' => 'div',
                'class' => 'row',
            ))
            ->panel('Opis', array(
                'label' => 'Opis',
                'active' => true,
                'general' => true,
                'class' => 'box-primary',
                'width' => 6,
            ))
            ->add('description', TextareaType::class, array(
                'label' => false,
                'required' => false,
                'attr' => array('rows' => 10),
            ))
            ->end()
            ->panel('Komponenty', array(
                'label' => 'Komponenty',
                'active' => true,
                'general' => true,
                'class' => 'box-primary',
                'width' => 6,
            ))

            ->add('components', ComponentsType::class, array(
                'entry_type' => ComponentType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false,
                'label'=>false,
            ))
            ->end()
            ->end()



        ;
    }

    protected function configureDatagridFilters(FiltersMapper $listMapper) {
        return $listMapper;
    }

    public function getTable(){
        return new ProductTable($this->getEntitymanager());
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('icon_filename', 'image', array('header' => 'Ikona'))
            ->addIdentifier('e_name', null, array('header' => 'Nazwa'))
            ->addIdentifier('e_shortName', null, array('header' => 'Krótka nazwa'))
            ->addIdentifier('a_active', 'bool', array('header' => 'Active'))
            ->addIdentifier('e_promo', 'bool', array('header' => 'Promocja'))
            ->addIdentifier('e_editable', 'bool', array('header' => 'Edytowalny'))
            ->addIdentifier('e_price', 'price', array('header' => 'Cena'))
            ->addIdentifier('e_super_price', 'price', array('header' => 'Cena promocyjna'))
            ->addIdentifier('e_id', 'link', array('header' => null, 'label'=>'edytuj', 'render' => function($id) {
                return $this->container->get('router')->generate(
                    'admin_edit', array('model' => 'product', 'id'=>$id)
                );
            }))
        ;
    }
    public function getGridType(){
        return 'table';
    }
}
