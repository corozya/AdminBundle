<?php

namespace Coro\AdminBundle\Admin;

use Coro\AdminBundle\DataGrid\DataGrid;
use Coro\AdminBundle\DataGrid\Filter;
use Coro\AdminBundle\DataGrid\ListMapper as GridListMapper;
use Coro\AdminBundle\Filter\ListMapper as FiltersMapper;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Coro\AdminBundle\Route\RouteCollection;

class Admin {

    protected $datagridBuilder;
    protected $datagrid;
    protected $form;
    protected $filter;
    protected $model;
    protected $container;
    protected $label;
    public $formtabs = array();
    public $maxPerPage = 10;

    public function __construct($code, $class, $baseControllerName, $container, $label) {
        $this->code = $code;
        $this->class = $class;
        $this->baseControllerName = $baseControllerName;
        $this->container = $container;
        $this->label = $label;
    }

    public function getListTemplate() {
        return 'CoroAdminBundle:Default:list.html.twig';
    }

    public function getClass() {
        return $this->class;
    }

    public function getModel() {
        if (!$this->model) {
            $this->buildModel();
        }
        return $this->model;
    }

    public function getBaseControllerName() {
        return $this->baseControllerName;
    }

    public function getContainer() {
        return $this->container;
    }

    public function getEntitymanager() {
        return $this->container->get('doctrine')->getManager();
    }

    public function getRepository() {
        return $this->getEntitymanager()->getRepository($this->class);
    }

    public function buildModel() {
        $this->model = $this->getRepository()->find(1);
    }

    public function getForm($model = null) {
        return $this->buildForm($model);
    }

    public function hasFilters() {
        return $this->filter ? true : false;
    }

    public function getFilters() {
        return $this->buildFilters();
    }


    public function getDatagrid() {
        $this->buildDatagrid();

        return $this->datagrid;
    }

    public function buildDatagrid() {
        if ($this->datagrid) {
            return $this->datagrid;
        }
        $mapper = new GridListMapper();
        $this->configureListFields($mapper);
        $this->datagrid = new DataGrid($mapper, $this->buildFilters(), $this);
        return $this->datagrid;
    }

    public function buildFilters($model = null) {
        if ($this->filter) {
            return;
        }
        $mapper = new FiltersMapper();
        $filters=$this->configureDatagridFilters($mapper);
        return $filters;
        $formBuilder = $this->getContainer()->get('form.factory')->createBuilder(FormType::class, $model, array('csrf_protection' => false));
        $formBuilder->setAction($this->generateUrl('admin_list', array('model' => $this->getBaseName())));



        $filter = new Filter($formBuilder, $mapper);
        return $filter;
    }


    public function getMaxPerPage() {
        return $this->maxPerPage;
    }

    public function getName() {
        $reflect = new \ReflectionClass($this);

        return strtolower($reflect->getShortName());
    }

    public function getRoutes() {
        $this->routes = new RouteCollection(
                $this->getBaseCodeRoute(), $this->getBaseRouteName(), $this->getBaseRoutePattern(), $this->getBaseControllerName(), $this->getBaseName()
        );

        $this->routes->add('list');
        $this->routes->add('create');
        $this->routes->add('edit', 'edit/{id}');
        return $this->routes;
    }

    public function getBaseCodeRoute() {
        return $this->getBaseRouteName();
    }

    public function getBaseName() {
        preg_match(self::CLASS_REGEX, $this->class, $matches);
        $name = $this->urlize($matches[5]);
        return $name;
    }

    public function getBaseRouteName() {
        preg_match(self::CLASS_REGEX, $this->class, $matches);
        $name = sprintf('%s_%s', 'admin', $this->urlize($matches[5])
        );
        return $name;
    }

    public function getBaseRoutePattern() {
        preg_match(self::CLASS_REGEX, $this->class, $matches);
        $pattern = sprintf('/%s/%s', empty($matches[1]) ? '' : $this->urlize($matches[1], '-') . '/', $this->urlize($matches[5], '-'));
        return $pattern;
    }

    public function urlize($word, $sep = '_') {
        return strtolower(preg_replace('/[^a-z0-9_]/i', $sep . '$1', $word));
    }

    public function getLabel() {
        return $this->label;
    }

    public function getGridConfig() {


        $out['type']=$this->getGridType();

        $out['filters']=[];

        //columns
        $mapper = new GridListMapper();
        $this->configureListFields($mapper);
        $out['columns']=[];

        foreach($mapper->getFields() as $field){
            $f=[];
            $f['name']=$field['name'];
            $f['type']=$field['type'];
            $f['mapper']=$field['mapper'];
            foreach($field['description'] as $key=>$item){
                if($key=='render'){
                    continue;
                }

                if(is_callable($item)){
                    $f['description'][$key]=$item();
                    continue;
                }
                $f['description'][$key]=$item;
//                var_dump($item);
            }


            $out['columns'][]=$f;
        }
        //filters
        $mapper = new FiltersMapper();
        $this->configureDatagridFilters($mapper);

        foreach ($mapper->getFields() as $key => $field) {
                $out['filters'][] = $field->createView();
        }
        return $out;
    }
    }

    protected function configureDatagridFilters(FiltersMapper $listMapper) {
        return $listMapper;
    }

    public function getLimit(){
        return 10;
    }
}
