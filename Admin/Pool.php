<?php

namespace Coro\AdminBundle\Admin;

class Pool {

    protected $container = null;

    public function __construct($container) {
        $this->container = $container;
    }

    public function getContainer() {
        return $this->container;
    }

    public function getAllAdminClasses() {
        $classes=array();
        foreach ($this->container->getServiceIds() as $id) {
            if (strpos($id, $this->getName()) === 0) {
                $classes[$id]=$this->container->get($id);
            }
        }
        return$classes;
    }

    public function getAdminByClass($class) {
        $container = $this->container->get($this->getName() . $class);

        return $container;
    }

    public function getName(){
        return 'admin.pool.';
    }
}
