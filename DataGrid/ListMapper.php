<?php

namespace Coro\AdminBundle\DataGrid;

class ListMapper {

    public $fields;

    public function __construct() {
        
    }

    public function add($name, $type = null, array $fieldDescriptionOptions = array()) {
        $fieldDescriptionOptions = array_merge(
                array('identifier' => false), $fieldDescriptionOptions);
        
        $this->fields[] = array(
            'name' => $name,
            'mapper' => false,
            'type' => $type,
            'description' => $fieldDescriptionOptions,
        );
        return $this;
    }

    public function addIdentifier($name, $type = null, array $fieldDescriptionOptions = array()) {
        $fieldDescriptionOptions = array_merge(
                array('identifier' => true), $fieldDescriptionOptions);

        $this->fields[] = array(
            'name' => $name,
            'mapper' => $name,
            'type' => $type,
            'description' => $fieldDescriptionOptions,
        );
        return $this;
    }

    public function getFields() {
        return $this->fields;
    }

}
