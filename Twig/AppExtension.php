<?php
namespace Coro\AdminBundle\Twig;
use Symfony\Component\PropertyAccess\PropertyAccess;
class AppExtension extends \Twig_Extension
{
        public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('md5', 'md5'),
        );
    }
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getAttribute', array($this, 'getAttribute'))
        );
    }
    public function md5($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }
    public function getAttribute($entity, $property) {
        return $this->accessor->getValue($entity, $property);
    }
    public function getName()
    {
        return 'app_extension';
    }
}