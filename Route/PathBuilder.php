<?php

namespace Coro\AdminBundle\Route;

use Coro\AdminBundle\Admin\Pool;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection as SymfonyRouteCollection;

class PathBuilder extends Loader {

    const ROUTE_TYPE_NAME = 'extra';

    /**
     * @var Pool
     */
    protected $pool;

    /**
     * @var array
     */
    protected $adminServiceIds = array();

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param Pool               $pool
     * @param array              $adminServiceIds
     * @param ContainerInterface $container
     */
    public function __construct(Pool $pool, array $adminServiceIds, ContainerInterface $container) {
        $this->pool = $pool;
        $this->adminServiceIds = $adminServiceIds;
        $this->container = $container;
    }

    /**
     */
    public function supports($resource, $type = null) {
        return $type === self::ROUTE_TYPE_NAME;
    }

    /**
     */
    public function load($resource, $type = null) {

        $collection = new SymfonyRouteCollection();

        foreach ($this->pool->getAllAdminClasses() as $key => $adminClass) {
            foreach ($adminClass->getRoutes()->getElements() as $key => $route) {
                $collection->add($key, $route);
            }
        }

        $path = '/{model}/list';
        $defaults = array(
            '_controller' => 'CoroAdminBundle:CRUD:list',
        );
        $route = new Route($path, $defaults, []);
        $collection->add('admin_list', $route);
        
        $path = '/{model}/grid';
        $defaults = array(
            '_controller' => 'CoroAdminBundle:CRUD:grid',
        );
        $route = new Route($path, $defaults, []);
        $collection->add('admin_grid', $route);

        $path = '/{model}/edit/{id}';
        $defaults = array(
            '_controller' => 'CoroAdminBundle:CRUD:list',
        );
        $route = new Route($path, $defaults, []);
        $collection->add('admin_edit', $route);
        
        
        return $collection;
    }

}
