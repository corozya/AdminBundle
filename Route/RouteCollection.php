<?php

namespace Coro\AdminBundle\Route;

use Coro\AdminBundle\Admin\Pool;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection as SymfonyRouteCollection;

class RouteCollection {

    protected $baseRouteName = 'admin';
    protected $baseRoutePattern = 'admin';
    protected $baseCodeRoute = 'admin';
    protected $baseControllerName = 'CoroAdminBundle:CRUD';
    protected $modelName;
    public $elements = array();

    public function __construct($baseCodeRoute, $baseRouteName, $baseRoutePattern, $baseControllerName, $modelName)
    {
        $this->baseCodeRoute = $baseCodeRoute;
        $this->baseRouteName = $baseRouteName;
        $this->baseRoutePattern = $baseRoutePattern;
        $this->modelName = $modelName;
        $this->baseControllerName = $baseControllerName ? $baseControllerName : $this->baseControllerName;
        
    }

    public function getCollection() {
        $collection = new SymfonyRouteCollection();
    }
    public function getElements() {
        return $this->elements;
    }

    public function add($name, $pattern = null, array $defaults = array(), array $requirements = array(), array $options = array(), $host = '', array $schemes = array(), array $methods = array(), $condition = '') {
        $pattern = $this->baseRoutePattern . '/' . ($pattern ? : $name);
        $code = $this->getCode($name);
        $routeName = $this->baseRouteName . '_' . $name;

        if (!isset($defaults['_controller'])) {
            $defaults['_controller'] = $this->baseControllerName . ':' . $this->actionify($code);
            $defaults['model']=$this->modelName;
        }
        
        $this->elements[$this->getCode($name)] = 
            new Route($pattern, $defaults, $requirements, $options, $host, $schemes, $methods, $condition);
        
        return $this;
    }

    public function getCode($name) {
        if (strrpos($name, '.') !== false) {
            return $name;
        }

        return $this->baseCodeRoute . '.' . $name;
    }

    public function actionify($action) {
        if (($pos = strrpos($action, '.')) !== false) {
            $action = substr($action, $pos + 1);
        }

        if (strpos($this->baseControllerName, ':') === false) {
            $action .= 'Action';
        }

        return lcfirst(str_replace(' ', '', ucwords(strtr($action, '_-', '  '))));
    }
    
    public function getRoute($type){
        return $this->hasRoute($type) ? $this->elements[$type] : false;
    }
    
    public function hasRoute($type){
        return isset($this->elements[$type]);
    }

}
