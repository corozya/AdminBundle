<?php

namespace Coro\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('files', FileType::class, array(
                    'multiple' => true,
                ))
                ->add('Types', EntityType::class, array(
                    'class' => 'AppBundle:MediaTypes',
                    'label' => 'Typ',
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => array(
                        'class' => 'button-groups',
                    )
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
    }

}
