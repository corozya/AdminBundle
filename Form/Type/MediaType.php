<?php

namespace Coro\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\DataTransformerInterface;
use Coro\AdminBundle\Form\DataTransformer\MediaToNumberTransformer;
use Coro\AdminBundle\Form\DataTransformer\MediaToFilenameTransformer;

class MediaType extends AbstractType {

    private $manager;

    public function __construct(ObjectManager $manager) {
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $transformer = new MediaToNumberTransformer($this->manager);
        $builder->addModelTransformer($transformer);
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $filename = null;
        $fileId = null;

        if (null !== $form->getData()) {
            $filename = $form->getData()->getFileName();
            $fileId = $form->getData()->getId();
        }
        $view->vars['image_filename'] = $filename;
        $view->vars['image_id'] = $fileId;
        $view->vars['allow_add'] = $options['allow_add'];
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefined(array('image_name', 'image_filename'));
        $resolver->setDefaults(array(
            'image_filename' => null,
            'image_id' => null,
            'allow_add'=>false,
            'invalid_message' => 'The selected issue does not exist',
        ));
    }

    public function getParent() {
        return HiddenType::class;
    }

}
