<?php

namespace Coro\AdminBundle\Form\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Coro\AdminBundle\Form\DataTransformer\MediaToNumberTransformer;
use Coro\AdminBundle\Form\DataTransformer\MediaToFilenameTransformer;

class GridselectorType extends AbstractType {

    private $manager;

    public function __construct(ObjectManager $manager) {
        $this->manager = $manager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $transformer = new MediaToNumberTransformer($this->manager);
        $builder->addModelTransformer($transformer);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'admin' => null,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {

        $view->vars['admin'] = $options['admin'];
    }

    public function getParent() {
        return TextType::class;
    }

}
