<?php

namespace Coro\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\DataTransformerInterface;
use Coro\AdminBundle\Form\DataTransformer\MediaToNumberTransformer;

class TabType extends AbstractType {
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'active' => false,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        
        $view->vars['active'] = $options['active'];
        
    }


    public function getName() {
        return 'tab';
    }

}
