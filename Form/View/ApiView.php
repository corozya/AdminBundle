<?php

namespace Coro\AdminBundle\Form\View;

use Symfony\Component\Form\Form;

class ApiView {

    protected $form;

    public function createView($formView) {
        unset($formView->parent);
        unset($formView->vars['form']);

        foreach ($formView as $view) {
            $this->createView($view);
        }

        return $formView;
    }

}
