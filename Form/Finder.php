<?php

namespace Coro\AdminBundle\Form;

use Symfony\Component\Form\Form;

class Finder {

    protected $form;

    public function __construct(Form $form) {
        $this->form = $form;
    }

    public function find($name, $pool = null) {
        if (!$pool) {
            $pool = $this->form;
        }

        if ($pool->has($name)) {
            return $pool->get($name);
        }
        
        foreach($pool->all() as $item){
            if($item->has($name)){
                return $item->get($name);
            }
        }
    }

}
