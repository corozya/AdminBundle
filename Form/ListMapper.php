<?php

namespace Coro\AdminBundle\Form;

use Coro\AdminBundle\Form\Type\TabType;

class ListMapper {

    public $fields;
    public $currentTab;
    public $currentPanel;
    

    public function __construct() {
        
    }

    public function end(){
        $this->fields[] = array(
            'type' => 'end',
        );
        return $this;
    }

    public function tabs($name, array $fieldDescriptionOptions = array()) {
        $this->fields[] = array(
            'name' => $name,
            'type' => 'tabs',
            'description' => array_merge(
                    $fieldDescriptionOptions, array(
                'label' => $name,
                'inherit_data' => true)
            ),
        );
        return $this;
    }

    public function container($name, array $fieldDescriptionOptions = array()) {
        $this->fields[] = array(
            'name' => $name,
            'type' => 'container',
            'description' => array_merge(
                    $fieldDescriptionOptions, array(
                'label' => $name,
                'inherit_data' => true)
            ),
        );
        return $this;
    }

    public function tab($name, array $fieldDescriptionOptions = array()) {
        $this->fields[] = array(
            'name' => $name,
            'type' => 'tab',
            'description' => array_merge(
                    $fieldDescriptionOptions, array(
                'label' => $name,
                'inherit_data' => true)
            ),
        );
        return $this;
    } 

    public function panel($name, array $fieldDescriptionOptions = array()) {
        $this->fields[] = array(
            'name' => $name,
            'type' => 'panel',
            'description' => array_merge(
                    $fieldDescriptionOptions, array(
                'inherit_data' => true)
            ),
        );
        return $this;
    }

    public function add($name, $type = null, array $fieldDescriptionOptions = array()) {

        $this->fields[] = array(
            'name' => $name,
            'type' => $type,
            'description' => $fieldDescriptionOptions,
        );
        return $this;
    }

}
