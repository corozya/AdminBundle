<?php

namespace Coro\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\File;

use Api\Model\BaseTable;

class MenuController extends Controller {

    /**
     * @Route("/admin/api/menu", name="api_menu")
     * @Method({"GET"})
     */
    public function menuAction(Request $request) {

        $pool = $this->container->get('coro_admin.pool');
        $menu = array();
        foreach ($pool->getAllAdminClasses() as $name => $class) {
            $menu[] = array(
                'model' => str_replace('admin.pool.', '', $name),
                'label' => $class->getLabel(),
            );
        }

        $response = new JsonResponse($menu, 200, array());
        $response->setCallback($request->get('callback'));
        return $response;
    }

}
