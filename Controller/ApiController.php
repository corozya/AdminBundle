<?php

namespace Coro\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\File;
use Coro\AdminBundle\Form\View\ApiView;
use \ZipArchive;

use Api\Model\BaseTable;

class ApiController extends Controller {

    /**
     * Konfiguracja grid
     * @Route("/admin/api/{model}/grid", name="api_grid_config", options={"expose"=true}, )
     * @Method({"GET","POST"})
     */
    public function gridAction(Request $request, $model) {
        $admin = $this->get('coro_admin.pool')->getAdminByClass($model);

        $out = $admin->getGridConfig();

        $response = new JsonResponse();
        $response->setData($out);
        $response->setStatusCode(Response::HTTP_OK);


        $response->setCallback($request->get('callback'));
        $response->send();
        return $response;
    }


    /**
     * Dane dla grida
     * @Route("/admin/api/{model}", name="api_grid_data")
     * @Method({"GET","POST"})
     */
    public function listAction(Request $request, $model) {

        $admin = $this->get('coro_admin.pool')->getAdminByClass($model);
        $postFilters=$request->get('filters');

        $table=$admin->getTable();

        $params=[
            'page'=>$request->get('page',1),
            'limit'=>$request->get('limit',$admin->getLimit()),
        ];

        $table->setParams($params);


        //przekazanie danych do filtrów
        $filters=$admin->getFilters()->getFields();
        foreach($filters as $filter){
            if(!empty($postFilters[$filter->getName()])){
                $filter->setValue($postFilters[$filter->getName()]);
            }
        }

        $table->setFilters($filters);

        $data = array(
            'meta'=>[
                'limit' => (int)$table->getLimit(),
                'pages' => (int)$table->getPages(),
                'page' => (int)$table->getPage(),
                'results' => (int) $table->getTotalRows(),
            ],
            'data' => $table->getData()->getScalarResult(),
        );

        $response = new JsonResponse();
        $response->setData($data);
        $response->setCallback($request->get('callback'));
        return $response;
    }
}
