<?php

namespace Coro\AdminBundle\Filter\Type;


class TypeAdapter{

    public function getType($name, $type, $options){
            switch($type) {
                case 'choose':
                    return new ChooseType($name, $options);
                    break;
                case 'text':
                    return new TextType($name, $options);
                    break;
            }
    }

}
