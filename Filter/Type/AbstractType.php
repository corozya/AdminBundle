<?php

namespace Coro\AdminBundle\Filter\Type;


Class AbstractType implements Type{

    static $type="";
    protected $name;
    protected $options;
    protected $value;

    public function __construct($name, $options)
    {
        $this->name=$name;
        $this->options=$options;
    }


    public function getType(){
        return $this::$type;
    }

    public function getName(){
        return $this->name;
    }

    public function createView(){
        $viewData=[];
        $viewData['name']=$this->name;
        $viewData['type']=$this->getType();
        return $viewData;
    }

    public function setValue($value){
        $this->value=$value;
        return $this;
    }

    public function getValue(){
        return $this->value;
    }

    public function getOptions(){
        return $this->options;
    }

    public function getFilter(){
        return !empty($this->options['filter']) ? $this->options['filter'] : null;
    }

    public function filter($qb){
        if(!($this->getValue())){
            return $qb;
        }

    }
}
