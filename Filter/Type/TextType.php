<?php

namespace Coro\AdminBundle\Filter\Type;


Class TextType extends AbstractType {
    public static $type='TextType';

    public function filter($qb){
        if(!($this->getValue())){
            return $qb;
        }
        $filter=$this->getFilter();

        if(empty($filter)){
            return $this;
        }

        return $filter($qb, $this->getValue());

    }

}
