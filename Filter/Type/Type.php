<?php

namespace Coro\AdminBundle\Filter\Type;


interface Type{

    public function __construct($name, $options);

}
