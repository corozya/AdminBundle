<?php

namespace Coro\AdminBundle\Filter\Type;


Class ChooseType extends AbstractType {

    public static $type='ChooseType';

    public function createView(){
        $viewData=[];
        $viewData['name']=$this->name;
        $viewData['type']=$this->getType();
        $viewData['choices']=$this->options['items']();
        return $viewData;
    }

    public function filter($qb){
        if(!($this->getValue())){
            return $qb;
        }
        $filter=$this->getFilter();

        if(empty($filter)){
            return $this;
        }

        return $filter($qb, $this->getValue());

    }
}
