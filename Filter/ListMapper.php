<?php

namespace Coro\AdminBundle\Filter;

use Coro\AdminBundle\Form\Type\TabType;
use Coro\AdminBundle\Filter\Type\TypeAdapter;

class ListMapper {

    protected $fields=[];

    public function add($name, $type = null, array $fieldDescriptionOptions = array()) {

        $this->fields[] = array(
            'name' => $name,
            'type' => $type,
            'description' => $fieldDescriptionOptions,
        );
        return $this;
    }

    public function getFields(){

        $fields=[];
        $typeAdapter=new TypeAdapter();

        foreach ($this->fields as $field){
            $fields[]=$typeAdapter->getType($field['name'], $field['type'], $field['description']);
        }
        return $fields;
    }

}
